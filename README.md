# Info

This project was created while following the Udemy course on Spring.

MVC Spring app with Hibernate, implementing CRUD, front-end Thymeleaf.

## What was covered

* Hibernate configuration
* pom configuration
* db connection


## Outcomes on local machine

* git installed
* java jdk installed
* Intellij IDEA installed
* atom installed
* created account on Bitbucket
* added SSH for Bitbucket (works in a funny way)
* tried to create a branch, implement feature there, merge to master
* installed ubuntu WSL (terminal)
* MySQL servere and Workbanch installed
* Postman installed

## Questions

1. git, bash git on Windows doesn't work. git from ubutntu wsl yes, but ssh connection needs to be repeated
1. how to have different READMEs for branch and master
  * now - modified readme, pushed only to branch not on master
  * what about the future? if add ., and afterwards commit and pushed ??
