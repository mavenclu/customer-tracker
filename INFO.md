# Branch List Customers

Implemented the feature of pulling the data form DB and displaying it on Thymeleaf template

### On this branch was done:

* Hibernate configuration via annotations
* Customer class added
* starting the DAO (for this branch only 1 method findAll())
* Thymeleaf template with table
