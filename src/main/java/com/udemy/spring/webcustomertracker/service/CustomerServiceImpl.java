package com.udemy.spring.webcustomertracker.service;

import com.udemy.spring.webcustomertracker.dao.CustomerDAO;
import com.udemy.spring.webcustomertracker.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDAO customerRepo;


    @Override
    @Transactional
    public List<Customer> getCustomers() {
        return customerRepo.findAll();
    }

    @Override
    @Transactional
    public void saveCustomer(Customer customer) {
        customerRepo.save(customer);
    }
}
