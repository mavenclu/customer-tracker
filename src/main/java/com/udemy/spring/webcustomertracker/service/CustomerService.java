package com.udemy.spring.webcustomertracker.service;

import com.udemy.spring.webcustomertracker.domain.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

}
