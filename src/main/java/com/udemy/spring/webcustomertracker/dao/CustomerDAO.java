package com.udemy.spring.webcustomertracker.dao;

import com.udemy.spring.webcustomertracker.domain.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> findAll();

    void save(Customer customer);
}
