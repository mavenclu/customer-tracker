package com.udemy.spring.webcustomertracker.controller;

import com.udemy.spring.webcustomertracker.domain.Customer;
import com.udemy.spring.webcustomertracker.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerServ;

    @GetMapping("/list")
    public String showCustomers(Model model) {

        model.addAttribute("customers", customerServ.getCustomers());
        return "customers-table";
    }

    @GetMapping("/add")
    public String showAddForm(Model model) {
        Customer theCustomer = new Customer();
        model.addAttribute("customer", theCustomer);
        return "customers/add-customer-form";
    }

    @GetMapping("/edit")
    public String showEditForm() {
        return "update-customer-form";
    }

    @PostMapping("/save")
    public String saveCustomer(@ModelAttribute("customer") Customer theCustomer) {
        customerServ.saveCustomer(theCustomer);
        return "redirect:/customers/list";

    }

}
